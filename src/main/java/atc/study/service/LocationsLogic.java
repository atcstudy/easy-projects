package atc.study.service;

import atc.study.dao.LocationsDao;
import atc.study.dao.LocationsDaoH2Impl;
import atc.study.jdo.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

public class LocationsLogic {

    private static final Logger log = LoggerFactory.getLogger(LocationsLogic.class);
    private static final Long COUNTY_LOCATION_TYPE = 1L;

    private LocationsDao locationsDao;

    public LocationsLogic(LocationsDao locationsDao) {
        this.locationsDao = locationsDao;
    }

    @PostConstruct
    public void init() {
        locationsDao.createLocationsTable();
    }

    public String getCountries() {
        log.info("getCountries has been called");
        List<Location> locations = locationsDao.getLocationsByTypeId(COUNTY_LOCATION_TYPE);

        return locations.stream()
                .map(Location::getLocationName)
                .collect(Collectors.joining(", "));
    }

}
