package atc.study.utils;

import atc.study.jdo.Location;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class TestUtils {

    public static List<Location> getLocationsByNameList(List<String> names){
        return names.stream().map(Location::new).collect(Collectors.toList());
    }
}
