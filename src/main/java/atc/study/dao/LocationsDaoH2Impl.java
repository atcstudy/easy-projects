package atc.study.dao;

import atc.study.dao.mapping.LocationRowMapper;
import atc.study.jdo.Location;
import atc.study.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocationsDaoH2Impl implements LocationsDao {

    private static final Logger log = LoggerFactory.getLogger(LocationsDaoH2Impl.class);

    private static final String CREATE_LOCATIONS_TABLE = FileUtils.loadScriptFromFile("/sql/create/locations.sql");
    private static final String GET_LOCATIONS = FileUtils.loadScriptFromFile("/sql/locations/get-locations.sql");
    private static final String GET_LOCATIONS_BY_TYPE_ID = FileUtils.loadScriptFromFile("/sql/locations/get-locations-by-typeId.sql");

    @Override
    @Transactional
    public void createLocationsTable() {
        jdbcTemplate.execute(CREATE_LOCATIONS_TABLE);
    }

    @Autowired
    @Qualifier("targetTemplate")
    JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("namedParameterTargetTemplate")
    NamedParameterJdbcTemplate namedParameterTargetTemplate;

    @Override
    @Transactional
    public List<Location> getLocations() {
        log.info("calling getLocations");
        Map<String, Object> params = new HashMap<>();

        return namedParameterTargetTemplate.query(GET_LOCATIONS, params, new LocationRowMapper());
    }

    @Override
    @Transactional
    public List<Location> getLocationsByTypeId(Long typeId) {
        log.info("calling getLocations");
        Map<String, Object> params = new HashMap<>();
        params.put("locationType", typeId);

        return namedParameterTargetTemplate.query(GET_LOCATIONS_BY_TYPE_ID, params, new LocationRowMapper());
    }
}
