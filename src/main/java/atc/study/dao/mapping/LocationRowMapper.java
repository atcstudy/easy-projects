package atc.study.dao.mapping;

import atc.study.jdo.Location;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LocationRowMapper implements RowMapper<Location> {

    @Override
    public Location mapRow(ResultSet rs, int rowNum) throws SQLException {
        Location location = new Location();
        location.setLocationId(rs.getLong("locationId"));
        location.setLocationName(rs.getString("locationName"));
        location.setLocationType(rs.getLong("locationType"));
        location.setLocationOrder(rs.getLong("locationOrder"));
        return location;
    }
}
