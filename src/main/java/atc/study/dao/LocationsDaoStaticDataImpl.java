package atc.study.dao;

import atc.study.dao.mapping.LocationRowMapper;
import atc.study.jdo.Location;
import atc.study.utils.FileUtils;
import atc.study.utils.TestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocationsDaoStaticDataImpl implements LocationsDao {

    private static final Logger log = LoggerFactory.getLogger(LocationsDaoStaticDataImpl.class);

    private static final List<String> COUNTRY_LIST = Arrays.asList("Indonesia",
            "Dominican Republic", "France", "Ireland", "Scotland", "Thailand");

    @PostConstruct
    public void init() {
        createLocationsTable();
    }

    @Override
    @Transactional
    public void createLocationsTable() {

    }

    @Override
    @Transactional
    public List<Location> getLocations() {
        log.info("calling getLocations");
        return TestUtils.getLocationsByNameList(COUNTRY_LIST);
    }

    @Override
    @Transactional
    public List<Location> getLocationsByTypeId(Long typeId) {
        log.info("calling getLocationsByTypeId");
        return TestUtils.getLocationsByNameList(COUNTRY_LIST);
    }
}
