package atc.study.dao;

import atc.study.jdo.Location;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface LocationsDao {

    @Transactional
    void createLocationsTable();

    @Transactional
    List<Location> getLocations();

    @Transactional
    List<Location> getLocationsByTypeId(Long typeId);
}
