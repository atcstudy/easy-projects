package atc.study.jdo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Location {

    private Long locationId;
    @JsonProperty(value = "name", required = true)
    private String locationName;
    @JsonProperty(value = "typeId", required = true)
    private Long locationType;
    @JsonProperty(value = "order", required = true)
    private Long locationOrder;

    public Location() {

    }

    public Location(String name) {
        this.locationName = name;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Long getLocationType() {
        return locationType;
    }

    public void setLocationType(Long locationType) {
        this.locationType = locationType;
    }

    public Long getLocationOrder() {
        return locationOrder;
    }

    public void setLocationOrder(Long locationOrder) {
        this.locationOrder = locationOrder;
    }
}