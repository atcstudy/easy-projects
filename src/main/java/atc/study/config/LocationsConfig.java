package atc.study.config;

import atc.study.dao.LocationsDao;
import atc.study.dao.LocationsDaoH2Impl;
import atc.study.dao.LocationsDaoStaticDataImpl;
import atc.study.service.LocationsLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LocationsConfig {

    @Bean
    public LocationsDao locationsDaoH2Impl() {
        return new LocationsDaoH2Impl();
    }

    @Bean
    public LocationsDao locationsDaoStaticData() {
        return new LocationsDaoStaticDataImpl();
    }

    @Bean
    public LocationsLogic locationsLogic() {
        return new LocationsLogic(locationsDaoStaticData());
    }

}
