create schema if not exists cms;

drop table if exists cms.locations;

create table if not exists cms.locations (
   locationId number(100000000),
   locationName varchar2(100),
   locationType number(10000),
   locationOrder number(10000000)
);


CREATE SEQUENCE	IF NOT EXISTS locationIdSequence START WITH 10;

insert into cms.locations (locationId, locationName, locationType, locationOrder)
values (43, 'Индонезия', 1, 1);

insert into cms.locations (locationId, locationName, locationType, locationOrder)
values (46, 'Домниникана', 1, 1);

insert into cms.locations (locationId, locationName, locationType, locationOrder)
values (49, 'Франция', 1, 1);

insert into cms.locations (locationId, locationName, locationType, locationOrder)
values (55, 'Ирландия', 1, 1);

insert into cms.locations (locationId, locationName, locationType, locationOrder)
values (32, 'Шотландия', 1, 1);

insert into cms.locations (locationId, locationName, locationType, locationOrder)
values (45, 'Египет', 1, 1);

insert into cms.locations (locationId, locationName, locationType, locationOrder)
values (11, 'Греция', 1, 1);

insert into cms.locations (locationId, locationName, locationType, locationOrder)
values (22, 'Россия', 1, 1);

insert into cms.locations (locationId, locationName, locationType, locationOrder)
values (35, 'Тайланд', 1, 1);

insert into cms.locations (locationId, locationName, locationType, locationOrder)
values (221, 'Москва', 2, 2);

insert into cms.locations (locationId, locationName, locationType, locationOrder)
values (335, 'Пекин', 2, 2);

insert into cms.locations (locationId, locationName, locationType, locationOrder)
values (327, 'Сан-Франциско', 2, 2);

insert into cms.locations (locationId, locationName, locationType, locationOrder)
values (4567, 'Эрмитаж', 3, 3);