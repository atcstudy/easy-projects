select locationId as locationId,
locationName as locationName,
locationType as locationType,
locationOrder as locationOrder
from cms.locations where locationType = :locationType;